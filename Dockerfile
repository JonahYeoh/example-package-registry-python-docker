# Use an official Python runtime as a parent image
FROM python:3.10-alpine  # Choose the version you need!

# Set environment variables
ENV PYTHONUNBUFFERED 1

# Create a directory for the app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Install any needed packages specified in requirements.txt
COPY requirements.txt /app/requirements.txt

# Copy pip.conf to the appropriate location
COPY pip.conf /root/.pip/pip.conf  # <- Very important!!!

# Set up pip to use the private GitLab package registry
RUN pip install --upgrade pip

# Install the private package and other dependencies
RUN pip install -r requirements.txt

# Run the application
CMD ["python", "main.py"]
