# With the presence of `pip.conf`, `requirements.txt` can be used like usual.
# `pip.conf` will resolve on where to find the required packages.
<package-name>==<package-version>
